

let op = document.getElementById('count');
let result = document.getElementById('result')

const operations = "/x-+%".split('');
const numbers = "0123456789";

function argument (i) {
    op.value = validate(''+op.value, '' + i);
    // console.log(op.value);
    if(op.value[op.value.length - 1]==='=') {
        result.value = calculate(op.value.substr(0, op.value.length-1))
    }

    if(i=="c") {
        result.value = 0
    }

}

// 0 6 => 6
// 0 . => 0.
// 0. = => 0.0
// ( . => (0.
// 0 ) => 0
// (0 = =>(0)=
  // 0+ * => 0*
// ( ( => (
// 0.52 . => 0.52
// /0 = => /0
  // 100 % => 100/100
function validate(a, b) {
    if(b=='c') {
        return "0";
    }

    if(a[a.length-1]=='=') {
        return validate('0', b);
    }

    a = a+'';
    b = b+'';
    var lastNumber = "";
    var lastOperator = ""
    var bracketClosed = true;
    a.split('').forEach(val=>{
        if(operations.indexOf(val)>=0) {
            lastOperator = val;
            lastNumber = "";
        } else if(val == "(") {
            bracketClosed = false;
            lastNumber = "";
            lastOperation = val;
        } else if(val==")") {
            bracketClosed = true;
            lastNumber = "";
            lastOperation = val;
        } else {
            lastNumber += val;
        }
    })

    if(b=="=") {
        if ((lastNumber == "" || lastNumber == "0") && lastOperator == "/") {
            return a;
        }
        if(!bracketClosed) {
            return a+")"+b;
        }
    }

    if(lastNumber == "" && "()".indexOf(lastOperator)<0 && b!="=") {
        if(operations.indexOf(b)>=0) {
            if(b=="%") b="/100";
            return a.substr(0, a.length-1) + b;
        }
        if(numbers.indexOf(b)>=0) {
            return a+b;
        }
    }

    if(lastNumber=="0") {
        if(b=="." || operations.indexOf(b)>=0) {
            return a+b;
        }
        return a.substr(0, a.length-1)+b
    }

    if(b == ".") {
        if(lastNumber.indexOf(".")<0) {
            return a+b;
        } else {
            return a;
        }
    }
    
    if(b=="(") {
        if(bracketClosed) {
            return a+b;
        } else {
            return a
        }
    }

    if (b==")") {
        if(bracketClosed) {
            return a;
        } 
        return a+b;
    }



    if(b=="%") b="/100";
    return a+b;

}

// 8+9 => 17
// 8-9*5 => -37
// (8+9)*5 => 85
// 1.2+2.2=> 3.4
function calculate(a) {
    a = a.replace("x", "*")
    return eval(a)
}

// console.log(calculate("8+9"))
// console.log(calculate("8+9x5"))
// console.log(calculate("(8+9)x5"))
// console.log(calculate("1.2+2.2"))
// console.log(calculate("(8+9)*(1+3)"))